﻿using Harmony;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NoTeleportCooldown
{
    public class MainPatcher
    {
        public static void Patch()
        {
            HarmonyInstance harmonyInstance = HarmonyInstance.Create("com.glibfire.graveyardkeeper.teleport.mod");
            harmonyInstance.PatchAll(Assembly.GetExecutingAssembly());
        }
    }
}
