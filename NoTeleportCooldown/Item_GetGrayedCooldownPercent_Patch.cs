﻿using Harmony;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoTeleportCooldown {
    [HarmonyPatch(typeof(Item))]
    [HarmonyPatch("GetGrayedCooldownPercent")]
    internal class Item_GetGrayedCooldownPercent_Patch {

        [HarmonyPrefix]
        public static bool Prefix(Item __instance, ref float __result)
        {
            bool result;
            if (__instance.id == "hearthstone")
            {
                __result = 0f;
                result = false;
            }
            else
            {
                result = true;
            }
            return result;
        }
    }
}
